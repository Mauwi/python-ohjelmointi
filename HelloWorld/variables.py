#Pythonissa muuttujille ei tarvitse määrittää tyyppiä,
# muuttujilla on silti tyyppi, alla on integer
int1 = 2
int2 = 5

result = int1+ int2
print(result)

result = int1 - int2
print(result)

result = int1*int2
print(result)

#Python palauttaa suoraan desimaali numeron eli flowting pointing
#jolloin integer voidaan jakaa integerillä jolloin flowting tulee automaattisesti

#Mikäli toivotaan kokonaislukua integerin jakamisesta niin silloin voidaan laittaa
#esim (/int1/int2)

result = int1/int2
print(result)

#Mikäli halutaan jatkaa laskutoimitusta niin silloin lisätään
#yhtäsuuruus merkin eteen se mitä halutaan tehdä


result //= int2 //int1
print(result)

float1= 2.5
float2 = 1.5

sum = float1 + float2
print(sum)

division=float1//float2
print(division)

#Booleanit kirjoitetaan suoraa isolla alkukirjaimella True/False

bool1= True
bool2 = False

#Eli tässä verrataan falsea trueksi, jolloin ohjelma tulostaa epätosi
#lisäsin alla olevaan bool1 or -> jolloin ohjelma tulostaa tosi
if bool1 or bool2:
    print("Tosi")
else:
    print("Epätosi")

#boolean on epätosi silloin kun arvo on NOLLA ja TOSI kun se on jotakin muuta kuin nolla
#jolloin voidaan kirjoittaa ylempään koodiin esim. int1 =1 if int1==1: -> tulee tosi

