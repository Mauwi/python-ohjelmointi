a = 10

while a >= 0:
    print(a)
    a -= 1 # Vähennetään a:n arvoa yhdellä
    # a = a-1 sama kuin yllä
    # a++ / a-- eivät toimi Pythonissa
else:
    print("")
    print("Silmukka päättyi")
    print("")

print("A:n arvo sovellukseen lopussa ", a)
print("\n")
    
cars = ["Volvo", "Toyota", "Fiat","Audi","Seat"]

for car in cars: #Syntaksi car tallennetaan auto listalta cars
    print("Auto:", car)

print("\n")

length = len(cars)
for index in range(0,length): # voi myös kirjoittaa range(0,len(cars)) 
    car = cars[index]
    print("Auto", car)