carInfo= {
    "brand":"Toyota",
    "model":"Avensis",
    "year": 2010,
    "price": 1266.5
}
print(carInfo)

#jos dictionary ei sisällä avainta, lisätään avvain-arvopari dictionaryyn
carInfo["color"]="red"
print(carInfo)

carInfo["year"] =2011
#Tällä tavoin pystymme päivittämään arvon jos avain on jo olemassa
print(carInfo)

price = carInfo["price"]
print("Hinta", price)

#Pop poistaa avainta vastaavan avain-parin, jos sellainen löytyy
#Jos arvoa ei löydy palautetaan default-arvo , jos sellainen on annettu
#parametrina. jos Default-arvoa ei ole annettu pop heittää virheen KeyError
carInfo.pop("price")
print(carInfo)

carInfo2 = {
    "brand" : "Jeep", #Auton merkki
    "model" : "Compass", #malli
    "year": 2018, #vuosimalli
    "price": 220002, #Euroina
    "color": "green"
}

#Listattiin dictionaryt, jotka sisälsivät autojen tiedot
carList = [carInfo, carInfo2]
print("")
print("")
print(carList,sep="\n")