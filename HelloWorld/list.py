carInfo = ["Volvo, 2019, 25200.95"]
carList = ["Toyta", "Volvo", "Volkswagen","Audi"]
print(carList)
print(carInfo)

car = carList[2]
print(car)

carList.append("Jeep") #Lisää alkion listan loppuun
carList.insert(2,"Fiat") #Lisää alkion tiettyyn listan indeksiin
print(carList)

remove = carList.pop() #Pop poistaa viimeisen alkion listasta ja palauttaa sen paluuarvona
print("Remove car", remove, sep="-") #korvaa välilyönnin viivalla
#\t on sama kuin tabulaattori ja \n on uusi rivi eli printteri
print(carList)

del carList[1:3] #del poistaa listan alkiot tietyissä indeksistä, sillä voidaan myös poistaa koko lista eli del carList
print(carList)

carList.remove("Audi")
print(carList)

der carList #Poistaa koko listan ja myös muuttujan
print(carList) #Tässä tulee virhe,carList-muuttujaa ei ole enää olemassa