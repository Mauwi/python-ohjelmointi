
print("Hello \nworld", end=" ")


#Merkkijono saa keskellä jonoa enterin.
# ja end kertoo että merkkijonon loppuu
# välilyöntiin

print("How \t are you")
# merkkijonon keskelle tulee pitkä
# välilyönti


print('just "kidding"')
#Merkkijonon sisälle saadaan hipsut tekstiin

print("\n")

print("\"things that\", 'have been done'")
#Merkkijonon sisälle saadaan pienet hipusut
#ja kaksoiset

print("\n")

#Samalle muuttujalle voi antaa useamman arvon
home = "Cat in a hat"
print(home)

home=1
print(home)
#Mutta silloin ne printtaa juuri sen arvon mikä on
# niin sanotusti uusin

jono="hei"

sum=jono+str(2)
print(sum)
#Stringiin pystyy lisäämään numeron jos laittaa numeron
# eteen str() merkin


# TÄSSÄ ON KOODIA
# name =input("What is your name > ")
# print("Hi " + name)



#Inputilla sä voit pyytää käyttäjältä muuttuja

# risuaidan saa nopeasti useammalle riville
# kun painaa control K ja tämän jälkeen control C

# tässä kohtaa vaihdetaan käyttäjän syötettä niin, että
# ensimmäinen kirjain olisi isolla (melkein sama asia kuin yläpuolella name)
print("\n")
example =input("what is your name > ")
# Muuttuja tulee kutsua uudelleen eli tua example
example = example.capitalize()
print("Hi "+ example)


# Nopesti useamman rivin risuaidan poisto tapahtuu
# control K control U

#onko example muuttujassa vähintään 1 kirjain ja onko ensimmäinen kirjain 
#kirjoitettu pienellä

if len(example) > 0 and example[0].islower:
    #Pythonissa on väliä sisännyksillä !!
    #esim. If lauseen sisältö pitää kirjoittaa sisennettynä
    example = example[0].upper()+ example[1:]
    #Viimeinen : on se miltä väliltä suurempi kirjain tulee löytää
    #Nämä on listoja ei arrayta

print("Hi" + example)




    #Saman voi kirjoittaa myös Näin 
    #originalExample = example
    #example = example[0].upper()
    #example +=originalExample[1:]
# print ("Hi" + example)



#Keno viivalla \ voi jatkaa pitkää tekstiä toiselle riville

esim = "useamman tekstin pituinen teksti asfkasokfoaskfaskfaspfkasfaspåfkaspasfa \
    jfjaisjigasjgasogjapskgjaopsjgaåfkawpåkfapwkfawjåawgkaogka \
        osakgoajsgoapsjgapoj"

print(esim)