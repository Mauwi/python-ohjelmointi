units = {"Tank","plane","Ship", "Dog"}
print(units)
#Printtaa taulukon eri järjestyksessä. Tää on ok tässä vaiheessa
#Se johtuu siitä miten taulukko on tallennettu, mutta me ei käydä läpi taulukon tallennusten menettelyä

units.add("Solder")
print(units)

units.update(["heavy tank", "helicopter"])
print(units)

#Discard poistaa alkion jos alkion ei ole setissä, discard ei aiheuta virhettä
units.discard("Dog")
print(units)
 #Vaikka poistettiin dog kahteen kertaa niin virhettä ei synny

 #Remove poistaa alkion ja heittää virheen, jos alkion ei löydy setistä
 units.remove("Solder")
 units.remove("Dog") #Tämä rivi heittää virheen
 print(units) #Koska ylempi rivi aiheutti vireeen tätä riviä ei suoriteta