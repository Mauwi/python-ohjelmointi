a = 0
b = 1

c = a == b # Tämä lause tarkistaa onko a yhtä suuri kuin b ja jos on niin ohjelma tallentaa c muuttujaan true ja epätosi mikäli ne eivät ole yhtäsuuret

print(c)

if a == b:
    print("a is equal to b")
    print("This belongs to the first if too")
else:
    print("a is not equal to b")